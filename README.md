# gnome-shell-extensions-enhancements
This package installs (by depending and recommending) most useful extensions for Gnome Shell and enables some extensions by default (after lots usability tests in educational and enterprise desktops).

Baltix GNU/Linux GNOME desktop uses some of these extensions by default, other Debian/Ubuntu based operating systems users which use GNOME can improve user experience by installing this metapackage.